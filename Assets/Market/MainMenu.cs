﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


[Serializable]
public class MainMenu {
		public Item[] content;

	public Item[] get_content(){
		return content;
	}

	public String store_id;
	public String store_App_url;
	public string get_store_id(){
		return store_id;
	}
	public string get_store_App_url(){
		return store_App_url;
	}

}
