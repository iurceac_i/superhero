﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class MainGame : MonoBehaviour {
	int countCoins;
	public Text afisareCoinuriInMarket;
	public Text coinsCounter;
	List<Skin> listSkins;
	public Sprite[] IconSkins;
	int countUnlockSpins;
	public NativeShareOnText nativShare;
	MainMenu itemsContent;
	// Use this for initialization
	void Start () {
		Application.targetFrameRate = 60;
//		PlayerPrefs.SetInt ("totalCoins",10000);
//		PlayerPrefs.DeleteAll ();
		ReadJson();
		countCoins = PlayerPrefs.GetInt ("totalCoins");
		coinsCounter.text = countCoins.ToString ();
		bool[] unlockSkins = PlayerPrefsX.GetBoolArray ("ListBoolValueSkins"); 
		bool isNull = unlockSkins == null || unlockSkins.Length == 0;

		listSkins = new List<Skin> ();
		listSkins.Add (new Skin (250,IconSkins [0],isNull ? false : unlockSkins[0]));
		listSkins.Add (new Skin (255,IconSkins [1],isNull ? true : unlockSkins[1]));
		listSkins.Add (new Skin (260,IconSkins [2],isNull ? true : unlockSkins[2]));
		listSkins.Add (new Skin (256,IconSkins [3],isNull ? true : unlockSkins[3]));
		listSkins.Add (new Skin (253,IconSkins [4],isNull ? true : unlockSkins[4]));
		listSkins.Add (new Skin (252,IconSkins [5],isNull ? true : unlockSkins[5]));
		listSkins.Add (new Skin (125,IconSkins [6],isNull ? true : unlockSkins[6]));
		listSkins.Add (new Skin (250,IconSkins [7],isNull ? true : unlockSkins[7]));
		listSkins.Add (new Skin (225,IconSkins [8],isNull ? true : unlockSkins[8]));
		listSkins.Add (new Skin (250,IconSkins [9],isNull ? true : unlockSkins[9]));
		listSkins.Add (new Skin (235,IconSkins [10],isNull ? true : unlockSkins[10]));
		listSkins.Add (new Skin (245,IconSkins [11],isNull ? true : unlockSkins[11]));
//		listSkins.Add (new Skin (245,IconSkins [12],isNull ? true : unlockSkins[12]));
//		listSkins.Add (new Skin (245,IconSkins [13],isNull ? true : unlockSkins[13]));
//		listSkins.Add (new Skin (245,IconSkins [14],isNull ? true : unlockSkins[14]));
//		listSkins.Add (new Skin (245,IconSkins [15],isNull ? true : unlockSkins[15]));
//		listSkins.Add (new Skin (245,IconSkins [16],isNull ? true : unlockSkins[16]));
//		listSkins.Add (new Skin (245,IconSkins [17],isNull ? true : unlockSkins[17]));
	}

	public void AddCoins(int value){
		countCoins = PlayerPrefs.GetInt ("totalCoins");
		countCoins += value;
		coinsCounter.text = countCoins.ToString ();
		afisareCoinuriInMarket.text = countCoins.ToString ();
		PlayerPrefs.SetInt ("totalCoins", countCoins);
	}

	public void RemoveCoins(int value){
		countCoins -= value;
		coinsCounter.text = countCoins.ToString ();
		afisareCoinuriInMarket.text = countCoins.ToString ();
		PlayerPrefs.SetInt ("totalCoins",countCoins);
	}

	public List<Skin> GetSkins (){
		return listSkins;
	}

	public void changeSkin (Skin listParamSpiner){
		//spinnerObject.GetComponent<Image> ().sprite = listParamSpiner.get_image ();
	}

	public void UnlockItem(int position){
		listSkins [position].set_isLocked(false);
		SaveDataListSkins ();
		countUnlockSpins = PlayerPrefs.GetInt ("unlockSpinerCount");
		countUnlockSpins = countUnlockSpins + 1;
	}

	private void SaveDataListSkins(){
		bool[] unlockSkins = new bool[listSkins.Count];
		for(int i = 0; i <listSkins.Count;i++){
			unlockSkins [i] = listSkins [i].get_isLocked ();
		}
		PlayerPrefsX.SetBoolArray ("ListBoolValueSkins",unlockSkins);
	}

	void OnApplicationQuit(){
		
	}

	public void ReadJson(){
		string path = "/Raw/purchases.json";
		#if UNITY_EDITOR
		path = "/StreamingAssets/purchases.json";
		#endif

		string json = File.ReadAllText(Application.dataPath + path);
		itemsContent = JsonUtility.FromJson<MainMenu> (json); 

	}
	public string GetStoreID(){
		return itemsContent.get_store_id ();
	}
	public string GetStoreAppUrl(){
		return itemsContent.get_store_App_url ();
	}

	public MainMenu GetProducts(){
		return itemsContent;
	}

	public void ShareTest(){
		Time.timeScale = 1f;
		nativShare.ShareScreenshotWithText ("I like This Game");
	}
}
