﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ListItemsController : MonoBehaviour, Adapter.OnItemClickListener{
	
	public Adapter adapter;
//	public MainMenu menu;
	public MainGame mainRepository;

//	public Image playerSkinImg;
	public Image skinImgUnlock;
	public Image skinImgDone;
	public Image skinImgAlert;
	public Button unlockSpiner;
	public Button cancelUnlockSp;
	public Button doneUnlockedSp;
	public GameObject firstUnlockPanel;
	public GameObject doneUnlockedPanel;
	public GameObject listSkinsPanel;
	public GameObject alertPanel;
	public GameObject marketPanel;
	int curentPosition;
	int countCoins;
	int curentPrice;
	public Text priceSkins;
	// Use this for initialization

	Skin s;
	void OnEnable(){
		adapter.SetAdapter (mainRepository.GetSkins ());
		int position = PlayerPrefs.GetInt ("selectPosition");
		if(PlayerPrefs.GetInt ("selectedTab") == 0){
			adapter.SetSelectedSkin (position);
		}
		adapter.registerListener (this);
	}
	public void Start (){
	}
	#region OnItemClickListener implementation
	public void onItemClicked (int position)
	{
		s = mainRepository.GetSkins()[position];
		if(s.get_isLocked()){
			firstUnlockPanel.SetActive (true);
			skinImgUnlock.sprite = s.get_image();
			skinImgDone.sprite = s.get_image ();
			skinImgAlert.sprite = s.get_image ();
			priceSkins.text = s.get_price ().ToString ();
			curentPosition = position;
		}
		else{
			firstUnlockPanel.SetActive (false);
			PlayerPrefs.SetInt ("selectPosition", position);
			PlayerPrefs.SetInt ("selectedTab", 0);
			adapter.SetSelectedSkin (position);
		}
	}

	#endregion
	public void CancelUnlockPanel(){
		firstUnlockPanel.SetActive (false);
		doneUnlockedPanel.SetActive (false);
	}



	public void BuyUnlockSpiner(){
		countCoins = PlayerPrefs.GetInt ("totalCoins");
		curentPrice = mainRepository.GetSkins()[curentPosition].get_price ();
		if(countCoins >= curentPrice){
		mainRepository.RemoveCoins (curentPrice);
//			MainGame.UnlockItem (mainRepository.GetSkins () [curentPosition]);
		DoneUnlockedSpinner();
		firstUnlockPanel.SetActive (false);
		doneUnlockedPanel.SetActive (true);
		//		mainRepository.ShowAD ();
		}else{
			alertPanel.SetActive (true);
		}
	}

	public void DoneUnlockedSpinner(){
		s = mainRepository.GetSkins()[curentPosition];
		mainRepository.UnlockItem (curentPosition);
		doneUnlockedPanel.SetActive (false);
		//		listSkinsPanel.SetActive (false);
		adapter.UpdateAdapterWithPosition (curentPosition);
		adapter.SetSelectedSkin (curentPosition);
	}

	public void CancelAlertPanel(){
		alertPanel.SetActive (false);
		firstUnlockPanel.SetActive (false);
	}
	public void GoToMarketButton(){
		firstUnlockPanel.SetActive (false);
		alertPanel.SetActive (false);
		listSkinsPanel.SetActive (false);
		marketPanel.SetActive (true);
	}
//	START ADS
//	#region AdsCallBack implementation
//	public void OnAdsSuccess ()
//	{
//		spinerImgUnlock.sprite = s.get_image();
//		firstUnlockPanel.SetActive (false);
//		unlockedPanel.SetActive (true);
//	}
//
//	public void OnAdsFail ()
//	{
//		
//	}
//	#endregion

	//End ADS


	//AdapterBS


}
