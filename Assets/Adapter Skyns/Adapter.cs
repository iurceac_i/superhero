﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Adapter : MonoBehaviour
{
	public GameObject gameObjectSP;
	private List<Skin> mainList;
	private OnItemClickListener listener;
	int oldSelected = -1;
	public void SetAdapter (List<Skin> listSkins){
		mainList = listSkins;
		for (int i = 0; i < mainList.Count; i++) {
			Skin s = mainList [i];
			GameObject priceColectionGO = gameObjectSP.transform.GetChild (3).gameObject;
			GameObject imageGameObject = gameObjectSP.transform.GetChild (0).gameObject;
			Image imageSkin = imageGameObject.GetComponent<Image> ();
			imageSkin.sprite = s.get_image ();
			GameObject btnIsUnlock = gameObjectSP.transform.GetChild (2).gameObject;
			Image imageBtnIsUnlock = btnIsUnlock.GetComponent<Image> ();
			if (s.get_isLocked ()) {
				priceColectionGO.SetActive (true);
				Color color = imageBtnIsUnlock.color;
				color.a = 1f;
				imageBtnIsUnlock.color = color;
			} else {
				priceColectionGO.SetActive (false);
				Color color = imageBtnIsUnlock.color;
				color.a = 0f;
				imageBtnIsUnlock.color = color;
			}
			GameObject priceGameObject = gameObjectSP.transform.GetChild (3).GetChild (2).gameObject;
			Text skinPrice = priceGameObject.GetComponent<Text> ();
			skinPrice.text = (s.get_price ()).ToString ();

			GameObject finalObj = Instantiate (gameObjectSP, transform);
			finalObj.SetActive (true);
			finalObj.name = i.ToString ();
			Button bt = finalObj.transform.GetChild (4).gameObject.GetComponent<Button> ();
			bt.onClick.AddListener (() => OnItemClick (finalObj));
		}
	}

	public void OnDisable ()
	{
		foreach (Transform child in transform) {
			GameObject.Destroy (child.gameObject);
		}
		oldSelected = -1;
	}

	public void registerListener (OnItemClickListener callback)
	{
		this.listener = callback;
	}

	private void OnItemClick (GameObject go)
	{
		int position = Int32.Parse (go.name);
		if (listener != null) {
			listener.onItemClicked (position);
		}
	}

	public interface OnItemClickListener
	{
		void onItemClicked (int position);
	}

	public void UpdateAdapterWithPosition (int position)
	{
		GameObject curentGO = transform.GetChild (position).gameObject;
		GameObject priceColectionGO = curentGO.transform.GetChild (3).gameObject;
		GameObject btnIsUnlock = curentGO.transform.GetChild (2).gameObject;
		Image imageBtnIsUnlock = btnIsUnlock.GetComponent<Image> ();
		if (mainList [position].get_isLocked ()) {
			priceColectionGO.SetActive (true);
			Color color = imageBtnIsUnlock.color;
			color.a = 1f;
			imageBtnIsUnlock.color = color;
		} else {
			priceColectionGO.SetActive (false);
			Color color = imageBtnIsUnlock.color;
			color.a = 0f;
			imageBtnIsUnlock.color = color;
		}
	}

	public void SetSelectedSkin (int position)
	{
		if (oldSelected == -1) {
			setItemSelected (position, true);	
		} else {
			if (position != oldSelected) {
				setItemSelected (position, true);
				setItemSelected (oldSelected, false);
			}
		}
		oldSelected = position;
		PlayerPrefs.SetInt ("selectedSkin",position);
		Debug.Log (position);
	}

	private void setItemSelected (int posSelect, bool selected)
	{
		GameObject curentGO = transform.GetChild (posSelect).gameObject;
		GameObject btnSelected = curentGO.transform.GetChild (1).gameObject;
		btnSelected.SetActive (selected);

	}

}
