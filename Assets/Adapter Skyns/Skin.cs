﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skin
{
	private	Sprite image;
	private	int price;
	private bool isLocked;
	public Skin (int priceSkin, Sprite img, bool isLocked){
		price = priceSkin;
		image = img;
		this.isLocked = isLocked;
}
	public Skin ( Sprite img)
	{
		image = img;
		isLocked = false;
	}
	public Sprite get_image ()
	{
		return image;
	}
	public int get_price ()
	{
		return price;
	}
	public bool get_isLocked ()
	{
		return isLocked;
	}
	public void set_isLocked(bool param){
		isLocked = param;
	}

}
