﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MyStats : MonoBehaviour {
	public GameObject FollowPanelInfo;
	public GameObject LikePanelInfo;
	string followerTotal;
	string likesTotal;
	public Text followTextInfo;
	public Text LikeTextInfo;
	// Use this for initialization
	void Start () {
		FollowPanelInfo.SetActive (false);
		LikePanelInfo.SetActive (false);



	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnClickInfoFollowButton(){
		followerTotal = PlayerPrefs.GetInt ("TotalKils").ToString ();
		followTextInfo.text = "Your Team killed " + followerTotal.ToString () + " Monsters";
		FollowPanelInfo.SetActive (true);
	}

	public void OnClickInfoLikeButton(){
		likesTotal = PlayerPrefs.GetInt ("TotalFlags").ToString ();
		LikeTextInfo.text = "Your Team captured " + likesTotal.ToString () + " Flags";
		LikePanelInfo.SetActive (true);
	}

	public void OnCloseButtonFollowInfo(){
		FollowPanelInfo.SetActive (false);
	}
	public void OnCloseButtonLikePanelInfo(){
		LikePanelInfo.SetActive (false);
	}

}
