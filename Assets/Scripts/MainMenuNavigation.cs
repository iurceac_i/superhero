﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Kakera;

public class MainMenuNavigation : MonoBehaviour {
	public GameObject mapPanel;
	public GameObject marketPanel;
	public GameObject skinsPanel;
	public GameObject myScorePanel;
	public Text followers;
	public Text likes;
	public Image imageStats;
	public Sprite defaultImage;
	string pathImageStats;
	public PickerController pickerContd;
	string followerTotal;
	string likesTotal;
	public MainGame mainGame;
	int lvl1;



	public void Start(){
		AppLovin.PreloadInterstitial();
		AppLovin.SetSdkKey("lF1FL5NLs3IrsqOTUqmMvnMLvLqvfJ5OZ7dvC0F34XvwMHTs6EC__ZKuw9oZTuy1_axCbAFfi8a3Q2wXG7WGgs");
		AppLovin.InitializeSdk ();
		AppLovin.LoadRewardedInterstitial();
		mapPanel.SetActive (false);
		marketPanel.SetActive (false);
		skinsPanel.SetActive (false);
		myScorePanel.SetActive (false);
	}



	public void OnSelectedMapPanel(){
		mapPanel.SetActive (true);
	}
	public void OnSelectedMarketPanel(){
		marketPanel.SetActive (true);
	}
	public void OnSelectedskinsPanel(){
		skinsPanel.SetActive (true);
	}
	public void OnSelectedMyScorePanel(){
		followerTotal = PlayerPrefs.GetInt ("TotalKils").ToString ();
		likesTotal = PlayerPrefs.GetInt ("TotalFlags").ToString ();
		pathImageStats = PlayerPrefs.GetString ("pathImageStats");

		if(pathImageStats == ""){	
			imageStats.sprite = defaultImage;
		}else{
			
			StartCoroutine(LoadImageStats(pathImageStats, imageStats));
		}
		followers.text = followerTotal;
		likes.text = likesTotal;
		myScorePanel.SetActive (true);


	}
	private IEnumerator LoadImageStats(string path, Image output)
	{
		
		var url = "file://" + path;
		var www = new WWW(url);
		yield return www;
		var texture = www.texture;
		if (texture == null)
		{
			Debug.LogError("Failed to load texture url:" + url);
		}

		output.sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
	}
	public void BackMapButton(){
		mapPanel.SetActive (false);
	}
	public void BackMarketButton(){
		marketPanel.SetActive (false);
	}
	public void BackSkinButton(){
		skinsPanel.SetActive (false);
	}
	public void BackMyScoreButton(){
		myScorePanel.SetActive (false);
	}


	public void OnRun1vs1tMap(){
		lvl1 = 1;
		PlayerPrefs.SetInt ("reloadMap",lvl1);
		PlayerPrefs.SetString ("levelStart","MultiLayer1");
		SceneManager.LoadScene(1);

	}
	public void OnRun1vs4tMap(){
		lvl1 = 2;
		PlayerPrefs.SetInt ("reloadMap",lvl1);
		PlayerPrefs.SetString ("levelStart","MultiLayer2");
		SceneManager.LoadScene(2);
	}
	public void OnRun1vs4AtMap(){
		lvl1 = 3;
		PlayerPrefs.SetInt ("reloadMap",lvl1);
		PlayerPrefs.SetString ("levelStart","MultiLayer3");
		SceneManager.LoadScene(3);
	}
	public void OnRun1vs4BtMap(){
		lvl1 = 4;
		PlayerPrefs.SetInt ("reloadMap",lvl1);
		PlayerPrefs.SetString ("levelStart","MultiLayer4");
		SceneManager.LoadScene(4);
	}


	public void ShowApplovinADS(){//+
		if(AppLovin.HasPreloadedInterstitial()){
			mainGame.AddCoins (5);
			// An ad is currently available, so show the interstitial.
			AppLovin.ShowInterstitial();

		}
		else{
			AppLovin.PreloadInterstitial();
			// No ad is available.  Perform failover logic...
		}
	}

}
