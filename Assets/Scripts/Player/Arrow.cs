﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityStandardAssets.CrossPlatformInput;

public class Arrow : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler {

	public bool isLeft;

	public void OnPointerExit (PointerEventData eventData)
	{
		CrossPlatformInputManager.SetAxis ("Horizontal", 0);
	}

	public void OnPointerUp (PointerEventData eventData)
	{
		CrossPlatformInputManager.SetAxis ("Horizontal", 0);
	}
		
	public void OnPointerDown (PointerEventData eventData)
	{
		CrossPlatformInputManager.SetAxis ("Horizontal", isLeft ? -1f : 1f);
	}
}
