﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuInScene : MonoBehaviour {
	public GameObject menuGame;
	public GameObject resumeBut;
	public GameObject pauseBut;
	int numReloadMap;
	// Use this for initialization
	void Start () {
		Time.timeScale = 1f;
		menuGame.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void OpenMenu(){
		
		menuGame.SetActive (true);
		PauseGame ();
	}

	public void ResumeGame(){
		PlayGame ();
		menuGame.SetActive (false);

	}

	public void PlayGame(){
		resumeBut.SetActive (false);
		pauseBut.SetActive (true);
		Time.timeScale = 1f;
	}

	public void PauseGame(){
		resumeBut.SetActive (true);
		pauseBut.SetActive (false);
		Time.timeScale = 0f;
	}

	public void RestartMissionGame(){
		numReloadMap = PlayerPrefs.GetInt ("reloadMap");
		SceneManager.LoadScene(numReloadMap, LoadSceneMode.Single);
	}
	public void MainMenuGame(){
		Time.timeScale = 1f;
		SceneManager.LoadScene(0);
	}
}
