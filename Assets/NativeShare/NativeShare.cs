﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using System.IO;

/*
 * https://github.com/ChrisMaire/unity-native-sharing
 */
using UnityEngine.UI;

public class NativeShare : MonoBehaviour {
	
	public string ScreenshotName = "screenshot.png";
//	public Sprite gameImage;
    public void ShareScreenshotWithText(string text)
    {
		
        string screenShotPath = Application.persistentDataPath + "/" + ScreenshotName;
        if(File.Exists(screenShotPath)) File.Delete(screenShotPath);
        Application.CaptureScreenshot(ScreenshotName);
        StartCoroutine(delayedShare(screenShotPath, text));

    }

    IEnumerator delayedShare(string screenShotPath, string text){
        while(!File.Exists(screenShotPath)) {
    	    yield return new WaitForSeconds(.05f);
        }
		Share(text, screenShotPath, "");
    }

public void Share(string shareText, string imagePath, string url, string subject = ""){
	#if UNITY_IOS
		CallSocialShareAdvanced(shareText, subject, url, imagePath);
	#else
		Debug.Log("No sharing set up for this platform.");
	#endif
}

#if UNITY_IOS
	public struct ConfigStruct
	{
		
		public string title;
		public string message;
	}

	[DllImport ("__Internal")] private static extern void showAlertMessage(ref ConfigStruct conf);

	public struct SocialSharingStruct
	{
		public string text;
		public string url;
		public string image;
		public string subject;
	}

	[DllImport ("__Internal")] private static extern void showSocialSharing(ref SocialSharingStruct conf);

	public static void CallSocialShare(string title, string message)
	{
		ConfigStruct conf = new ConfigStruct();
		conf.title  = title;
		conf.message = message;
		showAlertMessage(ref conf);
	}


	public static void CallSocialShareAdvanced(string defaultTxt, string subject, string url, string img)
	{
		SocialSharingStruct conf = new SocialSharingStruct();
		conf.text = defaultTxt;
		conf.url = url;
		conf.image = img;
		conf.subject = subject;

		showSocialSharing(ref conf);
	}
#endif
}
